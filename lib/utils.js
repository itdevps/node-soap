"use strict";

// var crypto = require('crypto');
exports.passwordDigest = function passwordDigest(nonce, created, password) {
  // digest = base64 ( sha1 ( nonce + created + password ) )
  var sha = new window.jsSHA(window.btoa(nonce || '') + created + password, 'TEXT');
  var pwHash = sha.getHash('SHA-1', 'B64');
  return pwHash;
  // var pwHash = crypto.createHash('sha1');
  // var rawNonce = new Buffer(nonce || '', 'base64').toString('binary');
  // pwHash.update(rawNonce + created + password);
  // return pwHash.digest('base64');
};

// exports.inherits = function inherits(ctor, superCtor) {
//   ctor.super_ = superCtor;
//   var TempCtor = function () {};
//   TempCtor.prototype = superCtor.prototype;
//   ctor.prototype = new TempCtor();
//   ctor.prototype.constructor = ctor;
// };
