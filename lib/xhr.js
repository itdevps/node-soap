"use strict";

window.getXmlHttpRequest = function() {
  try {
    if (window.XMLHttpRequest) {// Try native XmlHttp
      var xhr = new XMLHttpRequest();
      // some versions of Moz do not support the readyState property and the onreadystate event so we patch it!
      if (xhr.readyState == null) {
        xhr.readyState = 1;
        xhr.addEventListener("load", function() {
          xhr.readyState = 4;
          if (typeof xhr.onreadystatechange === "function") {
            xhr.onreadystatechange();
          }
        }, false);
      }
      return xhr;
    }
    if (window.ActiveXObject) { // Try IE XmlHttp
      if (SOAPProxyUtils._getXmlHttpRequest.progid) {
        return new ActiveXObject(SOAPProxyUtils._getXmlHttpRequest.progid);
      } else {
        var progids = ["Msxml2.XMLHTTP.6.0", "Msxml2.XMLHTTP.5.0", "Msxml2.XMLHTTP.4.0", "MSXML2.XMLHTTP.3.0", "MSXML2.XMLHTTP", "Microsoft.XMLHTTP"];
      }
      for (var i = 0; i < progids.length; i++) {
        try {
          var xhr = new ActiveXObject(progids[i]);
          SOAPProxyUtils._getXmlHttpRequest.progid = progids[i];
          return xhr;
        } catch (ex) {

        }
      }
    }
  } catch (ex) {

  }
  throw new Error("No XmlHttp support");
};
